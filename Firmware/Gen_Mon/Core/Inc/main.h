/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ESP_BOOT0_Pin GPIO_PIN_0
#define ESP_BOOT0_GPIO_Port GPIOA
#define ESP_BOOT1_Pin GPIO_PIN_1
#define ESP_BOOT1_GPIO_Port GPIOA
#define ESP_RST_Pin GPIO_PIN_2
#define ESP_RST_GPIO_Port GPIOA
#define CLK_8M192_OUT_Pin GPIO_PIN_3
#define CLK_8M192_OUT_GPIO_Port GPIOA
#define ADC_CS_Pin GPIO_PIN_4
#define ADC_CS_GPIO_Port GPIOA
#define SPI_SCK_ADC_Pin GPIO_PIN_5
#define SPI_SCK_ADC_GPIO_Port GPIOA
#define SPI_MISO_ADC_Pin GPIO_PIN_6
#define SPI_MISO_ADC_GPIO_Port GPIOA
#define SPI_MOSI_ADC_Pin GPIO_PIN_7
#define SPI_MOSI_ADC_GPIO_Port GPIOA
#define SYNC_Pin GPIO_PIN_0
#define SYNC_GPIO_Port GPIOB
#define READY_Pin GPIO_PIN_1
#define READY_GPIO_Port GPIOB
#define SPI_SCK_MEM_Pin GPIO_PIN_13
#define SPI_SCK_MEM_GPIO_Port GPIOB
#define SPI_MISO_MEM_Pin GPIO_PIN_14
#define SPI_MISO_MEM_GPIO_Port GPIOB
#define SPI_MOSI_MEM_Pin GPIO_PIN_15
#define SPI_MOSI_MEM_GPIO_Port GPIOB
#define DM_Pin GPIO_PIN_11
#define DM_GPIO_Port GPIOA
#define DP_Pin GPIO_PIN_12
#define DP_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define I2C_SCL_Pin GPIO_PIN_6
#define I2C_SCL_GPIO_Port GPIOB
#define I2C_SDA_Pin GPIO_PIN_7
#define I2C_SDA_GPIO_Port GPIOB
#define MEM_CS_Pin GPIO_PIN_9
#define MEM_CS_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
