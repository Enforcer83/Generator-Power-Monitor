//***************************************************************************************
//
// ADS131M04 4-Channel, Simultaneously-Sampling, 24-Bit, Delta-Sigma ADC Library
// Copyright (C) 2024 Jacob Putz, Integrated Microsystem Electronics, LLC
//
//---------------------------------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//---------------------------------------------------------------------------------------
//
// Developed for the STM32 family of microcontrollers <https://www.st.com>
//
//***************************************************************************************

#include "ADS131M04.h"

//***************************************************************************************
//***************************************************************************************
//
// Initialization functions
//
//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
//
// Intialization function when using an external clock source for the ADS131M04
//
//***************************************************************************************
uint8_t ADS131M04_Init_Ext_Clk(ADS131M04* adc, SPI_HandleTypeDef *spiHandle, GPIO_TypeDef *csBankADC, uint16_t csPinADC, GPIO_TypeDef *syncPinBank, uint16_t syncPin, GPIO_TypeDef *clkOutPinBank, uint16_t clkOutPin)
{

	//
	// Store interface parameters in struct
	//
	adc->_spiHandle = spiHandle;
	adc->_csBankADC = csBankADC;
	adc->_syncPinBank = syncPinBank;
	adc->_clkOutPinBank = clkOutPinBank;
	adc->_csPinADC = csPinADC;
	adc->_syncPin = syncPin;
	adc->_clkOutPin = clkOutPin;

	//
	// Initialize Struct variables.
	//
	adc->_externalClkADC = true;
	adc->_clkEnAssert = false;
	adc->_clkFreqADC = 81920000;
	adc->_reqClkFreqADC = 0;
	adc->readingADS = 0;

	adc->dataBitMode32 = false;
	adc->signExtend = false;
	adc->crcEnabled = false;
	adc->isANSI_CRC = false;
	adc->inputCRC = false;
	adc->regMapCRC = false;

	adc->_regSampleFreq = 0;
	adc->_sampleFreq = 8000;

	for (uint8_t i = 0; i < 10; i++) {

		adc->txBufDMA[i] = 0;
		adc->rxBufDMA[i] = 0;

		if (i < 4) {

			adc->resultADC[i] = 0;

		}

	}

	TimerDelayInit();

	return 0;

}

uint8_t begin_Ext_CLK(ADS131M04* adc, bool externalClkADC,  bool clkEnPolHi, uint32_t extClkFreq, uint32_t sampleFreq)
{

    // Clear DMA flags
	adc->readingADS = 0;

    //
    // If the external clock parameter is set to false then the wrong function was called
    //
    if (!adc->_externalClkADC) {

        Error_Handler();

    }

    uint8_t status = 0;
    uint16_t chipID = 0;
    uint16_t txBuf[10] = {0}, rxBuf[10] = {0};
    uint16_t clkRegVal = 0x0F02; // 0b0000111100000010
    uint32_t actOSR = 512;

    //
    // Ensure pins are in a known state
    //
    // Chip Select and Sync/Reset pins are active low so they must be set high.
    // Depending on the external clock source, the enable pin will be either hig or
    // low so a polarity must be specified (bool clkEnPolHi) to determine the enable pins
    // initial state.
    //
    HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);
    HAL_GPIO_WritePin(adc->_syncPinBank, adc->_syncPin, GPIO_PIN_SET);

    if (clkEnPolHi) {

        HAL_GPIO_WritePin(adc->_clkOutPinBank, adc->_clkOutPin, GPIO_PIN_RESET);
        adc->_clkEnAssert = false;

    }

    else {

        HAL_GPIO_WritePin(adc->_clkOutPinBank, adc->_clkOutPin, GPIO_PIN_SET);
        adc->_clkEnAssert = false;

    }

    //
    // Disable the external clock
    //
    HAL_GPIO_TogglePin(adc->_clkOutPinBank, adc->_clkOutPin);
    adc->_clkEnAssert = false;

    //
    // Verify the correct chip is connected
    //
    txBuf[0] = ADS_CMD_RESET;

	// Perform soft reset
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_RESET);
	status = (HAL_SPI_TransmitReceive(adc->_spiHandle, txBuf, rxBuf, 8, HAL_MAX_DELAY) == HAL_OK);
	HAL_GPIO_WritePin(adc->_csBankADC, adc->_csPinADC, GPIO_PIN_SET);

	HAL_Delay(250);

	// Verify we are communicating with the correct device
	chipID = readRegister(adc, ADS_REG_ID);

	if ((chipID & 0xFF00) != ADS131M04_ID) {

		return 0;

	}

	//
	// MODE Register
	//
	// Configure the MODE register so no register map or SPI input CRC is generated,
	// clear the reset status in the STATUS register, Change the data word length to
	// 32-bits MSB sign extended, SPI timeout is enabled, DRDY output is the logical or
	// of all channels, is high when no data is available and low when data is available.
	//
	// The above results in a 16-bit data word of 0x0314 in hex or 0b0000001100010100 in
	// binary.
	//
	// Datasheet pages 50 and 51
	//
	status += writeRegister(adc, ADS_REG_MODE, 0x0314);
	adc->dataBitMode32 = true;
	adc->signExtend = true;
	adc->crcEnabled = false;

	//
	// If the sampling frequency or input clock frequency are not equal to 8kHz and
	// 8.192MHz, respectively, an new Over Sampling Ratio (OSR) will need to be found
	//
	// fs = fmod / OSR = fclkin / (2 * OSR)
	//
	// fs     - sampling frequencing
	// fmod   - modulator frequency
	// fclkin - input clock frequency
	//
	if (sampleFreq != 8000 || extClkFreq != 8192000) {

		actOSR = extClkFreq / ( 2 * sampleFreq);

		if (actOSR < 96) {

			// OSR = 64
			clkRegVal |= (0x0008 << 2);
			adc->_clkFreqADC = extClkFreq;
			adc->_sampleFreq = extClkFreq / (2 * 64);

		}

		else if (actOSR > 192 && actOSR <= 384) {

			// OSR = 256
			clkRegVal |= (0x0001 << 2);
			adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 256);

		}

		else if (actOSR > 384 && actOSR <= 768) {

			// OSR = 512
            clkRegVal |= (0x0002 << 2);
            adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 512);

        }

        else if (actOSR > 768 && actOSR <= 1536) {

            // OSR = 1024
            clkRegVal |= (0x0003 << 2);
            adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 1024);

        }

        else if (actOSR > 1536 && actOSR <= 3072) {

            // OSR = 2048
            clkRegVal |= (0x0004 << 2);
            adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 2048);

        }

        else if (actOSR > 3072 && actOSR <= 6144) {

            // OSR = 4096
            clkRegVal |= (0x0005 << 2);
            adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 4096);

        }

        else if (actOSR > 6144 && actOSR <= 12288) {

            // OSR = 8192
            clkRegVal |= (0x0006 << 2);
            adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 8192);

        }

        else if (actOSR > 12288) {

            // OSR = 16256
            clkRegVal |= (0x0007 << 2);
            adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 16256);

        }

        else {

        	adc->_clkFreqADC = extClkFreq;
        	adc->_sampleFreq = extClkFreq / (2 * 128);

        }

	}

	else {

        clkRegVal = 0x0F0A;
        adc->_sampleFreq = sampleFreq;
        adc->_clkFreqADC = extClkFreq;

	}

	//
	// CLOCK Register
	//
	// Configure the CLOCK register so all ADC channels are enabled, turbo mode is
	// disabled, Over Sampling Ratio (OSR) is 512 and power mode is set to high
	// resolution.  With a clock input of 8.192 MHz this results in a sampling frequency
	// of 8 kHz.
	//
	// fs = fmod / OSR = fclkin / (2 * OSR)
	//
	// The above results in a 16-bit data word of 0x0F0A in hex or 0b0000111100001010 in
	// binary.
	//
	// Datasheet pages 52 and 53
	//
	status += writeRegister(adc, ADS_REG_CLOCK, clkRegVal);

	//
	// GAIN Register
	//
	// At reset, all programable gain amplifiers controlled by the GAIN register are set
	// to unity gain (1 V/V).  If a higher gain is needed on any channel, uncomment the
	// writeRegister() command below and provide the gain for the specific channel.  The
	// configuration shown is the default configuration.
	//
	// Datasheet pages 54 and 55
	//
	//status += writeRegister(ADS_REG_GAIN , 0x0000);

	//
	// CFG Register
	//
	// At reset, the global chop delay is 16, global chop is disabled, current detection
	// is set for any channel, the number of current detection thresholds exceeded is set
	// to 1, current detect measurements length is set to 128, and current mode detection
	// is disabled.  To change, uncomment the writeRegister() command below and provide
	// the desired configuration.  Utilizing current detection requires setting the
	// detection threshold.  If current detection is used, immediately set the threshold
	// using the setThrshReg() command after this function.  The configuration
	// shown is the default configuration.
	//
	// Datasheet pages 57 and 58
	//
	//status += writeRegister(ADS_REG_CFG , 0x0600);

	return status;

}

uint16_t readRegister(ADS131M04* adc, uint16_t regAddr)
{



	return 0;

}

uint8_t writeRegister(ADS131M04* adc, uint16_t regAddr, uint16_t data)
{

	uint8_t status = 0;

	return status;

}
